package org.gcube.documentstore.persistence;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.gcube.common.gxhttp.reference.GXConnection.HTTPMETHOD;
import org.gcube.common.gxhttp.request.GXHTTPStringRequest;
import org.gcube.common.resources.gcore.GCoreEndpoint;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;
import org.gcube.documentstore.records.DSMapper;
import org.gcube.documentstore.records.Record;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.gcube.resources.discovery.icclient.ICFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.ForbiddenException;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.WebApplicationException;

/**
 * @author Alessandro Pieve (ISTI - CNR) alessandro.pieve@isti.cnr.it
 * @author Luca Frosini (ISTI - CNR) luca.frosini@isti.cnr.it
 */
public class PersistenceAccountingService extends PersistenceBackend {

	private static final Logger logger = LoggerFactory.getLogger(PersistenceAccountingService.class);

	public static final String PATH_SERVICE_INSERT_ACCOUNTING = "/record";

	public static final String APPLICATION_JSON_CHARSET_UTF_8 = "application/json;charset=UTF-8";

	public static final String URL_PROPERTY_KEY = "URL";

	public static final String SERVICE_CLASS = "Accounting";
	public static final String SERVICE_NAME = "AccountingService";
	public static final String SERVICE_ENTRY_NAME = "org.gcube.accounting.service.AccountingResource";

	private static final String USER_AGENT = "document-store-lib-accounting-service";

	private static String FORCED_URL;

	protected String url;

	private static String classFormat = "$resource/Profile/ServiceClass/text() eq '%1s'";
	private static String nameFormat = "$resource/Profile/ServiceName/text() eq '%1s'";
	private static String statusFormat = "$resource/Profile/DeploymentData/Status/text() eq 'ready'";
	private static String containsFormat = "$entry/@EntryName eq '%1s'";

	private static SimpleQuery queryForService() {
		logger.trace("going to query GCoreEndpoint of {}", SERVICE_NAME);
		return ICFactory.queryFor(GCoreEndpoint.class).addCondition(String.format(classFormat, SERVICE_CLASS))
				.addCondition(String.format(nameFormat, SERVICE_NAME)).addCondition(String.format(statusFormat))
				.addVariable("$entry", "$resource/Profile/AccessPoint/RunningInstanceInterfaces/Endpoint")
				.addCondition(String.format(containsFormat, SERVICE_ENTRY_NAME)).setResult("$entry/text()");
	}

	protected static void forceURL(String url) {
		FORCED_URL = url;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void prepareConnection(PersistenceBackendConfiguration configuration) throws Exception {
		if(FORCED_URL!=null) {
			url = FORCED_URL;
		} else {
			url = configuration.getProperty(URL_PROPERTY_KEY);
			if (url == null || url.compareTo("") == 0) {
				logger.debug("Invalid URL provided from Configuration. Looking for RunningInstance.");
				SimpleQuery serviceQuery = queryForService();
				List<String> addresses = ICFactory.client().submit(serviceQuery);
				if (addresses == null || addresses.isEmpty()) {
					String error = String.format("No Running Instance %s:%s found in the current context", SERVICE_CLASS,
							SERVICE_NAME);
					throw new Exception(error);
				}
				Random random = new Random();
				int index = random.nextInt(addresses.size());
				url = addresses.get(index);
			}
		}
		logger.debug("Accounting Service URL to be contacted is {}", url);
	}

	protected StringBuilder getStringBuilder(InputStream inputStream) throws IOException {
		StringBuilder result = new StringBuilder();
		try(BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
			String line;
			while((line = reader.readLine()) != null) {
				result.append(line);
			}
		}
		return result;
	}

	protected String parseHttpURLConnection(HttpURLConnection connection) throws WebApplicationException {
		try {
			int responseCode = connection.getResponseCode();
			// String responseMessage = connection.getResponseMessage();
			
			if(connection.getRequestMethod().compareTo(HTTPMETHOD.HEAD.toString()) == 0) {
				if(responseCode == HttpURLConnection.HTTP_NO_CONTENT) {
					return null;
				}
				if(responseCode == HttpURLConnection.HTTP_NOT_FOUND) {
					throw new NotFoundException();
				}
				if(responseCode == HttpURLConnection.HTTP_FORBIDDEN) {
					throw new ForbiddenException();
				}
			}
			
			if(responseCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
				InputStream inputStream = connection.getErrorStream();
				StringBuilder result = getStringBuilder(inputStream);
				String res = result.toString();
				throw new WebApplicationException(res, responseCode);
			}
			
			StringBuilder result = getStringBuilder(connection.getInputStream());
			return result.toString();
		} catch (WebApplicationException e) {
			throw e;
		} catch (Exception e) {
			throw new WebApplicationException(e);
		} finally {
			connection.disconnect();
		}
	}
	
	protected void send(Record... records) throws Exception {
		List<Record> list = Arrays.asList(records);
		String body = DSMapper.marshal(list);
		
		logger.trace("Going to persist {}s {}", Record.class.getSimpleName(), body);

		
		GXHTTPStringRequest gxhttpStringRequest = GXHTTPStringRequest.newRequest(url);
		gxhttpStringRequest.path(PATH_SERVICE_INSERT_ACCOUNTING);
		gxhttpStringRequest.header("Content-Type", APPLICATION_JSON_CHARSET_UTF_8);
		Secret secret = SecretManagerProvider.get();
		Map<String, String> headers = secret.getHTTPAuthorizationHeaders();
		for(String name : headers.keySet()) {
			gxhttpStringRequest.header(name, headers.get(name));
		}
		gxhttpStringRequest.from(USER_AGENT);
		gxhttpStringRequest.withBody(body);
		
		HttpURLConnection httpURLConnection = gxhttpStringRequest.post();
		parseHttpURLConnection(httpURLConnection);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void reallyAccount(Record record) throws Exception {
		send(record);
	}

	@Override
	protected void accountWithFallback(Record... records) throws Exception {
		try {
			send(records);
		} catch (Throwable e) {
			super.accountWithFallback(records);
		}
	}

	@Override
	public void close() throws Exception {}

	@Override
	protected void openConnection() throws Exception {}

	@Override
	protected void closeConnection() throws Exception {}

	@Override
	public boolean isConnectionActive() throws Exception {
		return true;
	}

	@Override
	protected void clean() throws Exception {
		// TODO Auto-generated method stub
	}
	
}
