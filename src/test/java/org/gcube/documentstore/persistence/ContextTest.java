/**
 * 
 */
package org.gcube.documentstore.persistence;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.gcube.common.iam.D4ScienceIAMClient;
import org.gcube.common.iam.D4ScienceIAMClientAuthn;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.AccessTokenSecret;
import org.gcube.common.security.secrets.Secret;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContextTest {
	
	private static final Logger logger = LoggerFactory.getLogger(ContextTest.class);
	
	protected static final String CONFIG_INI_FILENAME = "config.ini";
	
	public static final String PARENT_DEFAULT_TEST_SCOPE;
	public static final String DEFAULT_TEST_SCOPE;
	public static final String ALTERNATIVE_TEST_SCOPE;
	
	public static final String GCUBE;
	// public static final String DEVNEXT;
	// public static final String NEXTNEXT;
	public static final String DEVSEC;
	public static final String DEVVRE;
	
	protected static final Properties properties;
	
	public static final String TYPE_PROPERTY_KEY = "type";
	public static final String USERNAME_PROPERTY_KEY = "username"; 
	public static final String PASSWORD_PROPERTY_KEY = "password"; 
	public static final String CLIENT_ID_PROPERTY_KEY = "clientId"; 
	
	public static final String RESOURCE_REGISTRY_URL_PROPERTY = "RESOURCE_REGISTRY_URL";
	
	static {
		GCUBE = "/gcube";
		// DEVNEXT = GCUBE + "/devNext";
		// NEXTNEXT = DEVNEXT + "/NextNext";
		DEVSEC = GCUBE + "/devsec";
		DEVVRE = DEVSEC + "/devVRE";
		
		PARENT_DEFAULT_TEST_SCOPE = GCUBE;
		DEFAULT_TEST_SCOPE = DEVSEC;
		ALTERNATIVE_TEST_SCOPE = DEVVRE;
		
		properties = new Properties();
		InputStream input = ContextTest.class.getClassLoader().getResourceAsStream(CONFIG_INI_FILENAME);
		try {
			// load the properties file
			properties.load(input);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	private enum Type{
		USER, CLIENT_ID
	};
	
	public static void set(Secret secret) throws Exception {
		SecretManagerProvider.reset();
		SecretManagerProvider.set(secret);
	}
	
	public static void setContextByName(String fullContextName) throws Exception {
		logger.debug("Going to set credentials for context {}", fullContextName);
		Secret secret = getSecret(fullContextName);
		set(secret);
	}
	
	
	protected static String getJWTAccessToken(String context) throws Exception {
		Type type = Type.valueOf(properties.get(TYPE_PROPERTY_KEY).toString());
		
		String accessToken = null;
		
		int index = context.indexOf('/', 1);
		String root = context.substring(0, index == -1 ? context.length() : index);
		
		D4ScienceIAMClient iamClient = D4ScienceIAMClient.newInstance(root);
		D4ScienceIAMClientAuthn d4ScienceIAMClientAuthn = null;
		switch (type) {
			case CLIENT_ID:
				String clientId = properties.getProperty(CLIENT_ID_PROPERTY_KEY);
				String clientSecret = properties.getProperty(root);
				
				d4ScienceIAMClientAuthn = iamClient.authenticate(clientId, clientSecret, context);
				break;
		
			case USER:
			default:
				String username = properties.getProperty(USERNAME_PROPERTY_KEY);
				String password = properties.getProperty(PASSWORD_PROPERTY_KEY);
				
				d4ScienceIAMClientAuthn = iamClient.authenticateUser(username, password, context);
				break;
				
		}
		accessToken = d4ScienceIAMClientAuthn.getAccessTokenString();
		
		logger.trace("Generated Access Token is {}", accessToken);
		return accessToken;
		
	}
	
	public static Secret getSecret(String context) throws Exception {
		String accessToken = getJWTAccessToken(context);
		Secret secret = new AccessTokenSecret(accessToken, context);
		return secret;
	}
	
	public static void setContext(String token) throws Exception {
		Secret secret = getSecret(token);
		set(secret);
	}
	
	public static String getUser() {
		String user = "Unknown";
		try {
			user = SecretManagerProvider.get().getOwner().getId();
		} catch(Exception e) {
			logger.error("Unable to retrieve user. {} will be used", user);
		}
		return user;
	}
	
	@BeforeClass
	public static void beforeClass() throws Exception {
		setContextByName(DEFAULT_TEST_SCOPE);
	}
	
	@AfterClass
	public static void afterClass() throws Exception {
		SecretManagerProvider.reset();
	}
	
}
