/**
 * 
 */
package org.gcube.documentstore.persistence;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.documentstore.records.DSMapper;
import org.gcube.documentstore.records.Record;
import org.gcube.documentstore.utility.TestUsageRecord;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class AccountingServiceTest extends ContextTest {

	private static final Logger logger = LoggerFactory.getLogger(AccountingServiceTest.class);
	
	protected static final String ACCOUNTING_SERVICE_FILENAME = "accounting-service.properties"; 
	public static final String ACCOUNTING_SERVICE_URL_PROPERTY = "ACCOUNTING_SERVICE_URL";
	public static String ACCOUNTING_SERVICE_URL;

    static {
		Properties properties = new Properties();
		
		try {
			InputStream input = AccountingServiceTest.class.getClassLoader().getResourceAsStream(ACCOUNTING_SERVICE_FILENAME);
			// load the properties file
			properties.load(input);
            ACCOUNTING_SERVICE_URL = properties.getProperty(ACCOUNTING_SERVICE_URL_PROPERTY);
		
            if(ACCOUNTING_SERVICE_URL!=null){
                PersistenceAccountingService.forceURL(ACCOUNTING_SERVICE_URL);
            }
		} catch (Throwable e) {
            // OK the url is read from IS
		}
		
	}

	protected PersistenceBackend persistenceBackend;

	// @Before
	// public void before() throws Exception {
	// 	PersistenceBackendConfiguration persitenceConfiguration = PersistenceBackendConfiguration
	// 			.getInstance(PersistenceAccountingService.class);
	// 	persistenceBackend = new PersistenceAccountingService();
	// 	persistenceBackend.prepareConnection(persitenceConfiguration);
		
	// 	logger.debug("Persistence Backend is {}", persistenceBackend.getClass().getSimpleName());
	// }

	@Before
	public void before() {
		String context = SecretManagerProvider.get().getContext();
		persistenceBackend = PersistenceBackendFactory.getPersistenceBackend(context);
		if(persistenceBackend instanceof FallbackPersistenceBackend) {
			persistenceBackend = PersistenceBackendFactory.discoverPersistenceBackend(context,
				(FallbackPersistenceBackend) persistenceBackend);
		}
		
		Assert.assertTrue(persistenceBackend instanceof PersistenceAccountingService);
		logger.debug("Persistence Backend is {}", persistenceBackend.getClass().getSimpleName());
		
	}

	@Test
	public void testSingleInsertService() throws Exception {
		Record record = TestUsageRecord.createTestServiceUsageRecord();
		persistenceBackend.accountWithFallback(record);
	}

	@Test
	public void testMultipleInsertService() throws Exception {
		int count = 2;
		Record[] records = new Record[count];
		for (int i = 0; i < count; i++) {
			records[i] = TestUsageRecord.createTestServiceUsageRecord();
		}
		
		List<Record> recordList = Arrays.asList(records);
		String ret = DSMapper.marshal(recordList);
		logger.debug(ret);
		
		persistenceBackend.accountWithFallback(records);

	}

	@Test
	public void testMultipleInsertStorage() throws Exception {

		int count = 10;
		Record[] records = new Record[count];
		for (int i = 0; i < count; i++) {
			records[i] = TestUsageRecord.createTestStorageUsageRecord();
		}
		persistenceBackend.accountWithFallback(records);

	}

	@Test
	public void testMultipleInsertJob() throws Exception {
		int count = 10;
		Record[] records = new Record[count];
		for (int i = 0; i < count; i++) {
			records[i] = TestUsageRecord.createTestJobUsageRecord();
		}
		persistenceBackend.accountWithFallback(records);

	}

	@Test
	public void testMultipleInsertPortlet() throws Exception {
		int count = 10;
		Record[] records = new Record[count];
		for (int i = 0; i < count; i++) {
			records[i] = TestUsageRecord.createTestPortletUsageRecord();
		}
		persistenceBackend.accountWithFallback(records);

	}
	
}