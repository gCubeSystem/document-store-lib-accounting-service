# Document Store Backend Connector Library for Accounting Service

Document Store Backend Connector Library for Accounting Service

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

[Document Store Backend Connector Library for Accounting Service](https://wiki.gcube-system.org/gcube/Document_Store_Library)

## Change log

See [Releases](https://code-repo.d4science.org/gCubeSystem/document-store-lib-accounting-service/releases).

## Authors

* **Luca Frosini** ([ORCID](https://orcid.org/0000-0003-3183-2291)) - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)

## How to Cite this Software

Tell people how to cite this software. 
* Cite an associated paper?
* Use a specific BibTeX entry for the software?


    @Manual{,
        title = {Document Store Backend Connector Library for Accounting Service},
        author = {{Frosini, Luca}},
        organization = {ISTI - CNR},
        address = {Pisa, Italy},
        year = 2019,
        url = {http://www.gcube-system.org/}
    } 

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes including:

- the Sixth Framework Programme for Research and Technological Development
    - DILIGENT (grant no. 004260).
- the Seventh Framework Programme for research, technological development and demonstration 
    - D4Science (grant no. 212488);
    - D4Science-II (grant no.239019);
    - ENVRI (grant no. 283465);
    - iMarine(grant no. 283644);
    - EUBrazilOpenBio (grant no. 288754).
- the H2020 research and innovation programme 
    - SoBigData (grant no. 654024);
    - PARTHENOS (grant no. 654119);
    - EGIEngage (grant no. 654142);
    - ENVRIplus (grant no. 654182);
    - BlueBRIDGE (grant no. 675680);
    - PerformFish (grant no. 727610);
    - AGINFRAplus (grant no. 731001);
    - DESIRA (grant no. 818194);
    - ARIADNEplus (grant no. 823914);
    - RISIS2 (grant no. 824091);


