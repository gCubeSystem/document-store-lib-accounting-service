This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Document Store Backend Connector Library for Accounting Service

## [v3.0.0-SNAPSHOT]

- Migrated code to comply with smartgears 4
- Moved to Java 11
- Upgraded maven-parent to 1.2.0
- Upgraded gcube-bom to 4.0.0
- Fixed test   

## [v2.0.0] 

- Switched JSON management to gcube-jackson [#19115]
- Added more logs to provide bugs investigation


## [v1.3.0] [r4.12.1] - 2018-10-10

- Added logs to provide bugs investigation


## [v1.2.0] [r4.10.0] - 2017-02-15

- Changed POST URLs due to change in service which has been made REST compliant [#10810]


## [v1.1.0] [r4.7.0] - 2017-10-09

- Added HTTPS support [#8758]
- Added HTTP Redirection support [#8758]
- Added HAProxy discovery support [#8758]
- Restored Fallback capabilities [#9016]
- Always posting a List of Record even when we have to post a single one [#9034]
- Record List is marshalled as JSON using jackson [#9036]
- Implemented isConnectionActive() in PersistenceBackend provided implementation

## [v1.0.0] [r4.5.0] - 2017-06-07

- First Release

